import { settings } from './settings';
import anime from 'animejs/lib/anime.es.js';

function preventDefault(e) {
  e.preventDefault();
}

export function disableScroll() {
  document.body.addEventListener("touchmove", preventDefault, {
    passive: false
  });
}

export function enableScroll() {
  document.body.removeEventListener("touchmove", preventDefault);
}

export function parallaxMouse() {
  const parallaxMouse = (parent, element) => {

    let _elements = parent.querySelectorAll(element);
    let _offset = parent.getBoundingClientRect();
    let _dataAxe = parent.dataset.paralaxAxe;

    parent.addEventListener('mousemove', (ev) => {
      const _mousePos = {
        top: ev.pageY - _offset.top,
        left: ev.pageX - _offset.left
      };

      Array.prototype.forEach.call(_elements, (el) => {
        const _elData = el.dataset.parallaxOffset;

        const _strength = parseInt(_elData);

        let _traX = (_mousePos.left - (_offset.width / 3)) * _strength / _offset.width;
        let _traY = (_mousePos.top - (_offset.height / 3)) * _strength / _offset.height;

        if (_dataAxe === 'x') {
          _traY = '0';
        } else if (_dataAxe === 'y') {
          _traX = '0';
        }

        el.style.transform = "translate(" + _traX + "px, " + _traY + "px)";
      });
    });
  };

  let _parallax = document.querySelector(".js-parallax");
  let _elements = ".js-parallaxItem";
  parallaxMouse(_parallax, _elements);
}