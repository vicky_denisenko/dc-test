import { parallaxMouse } from './functions';
import anime from 'animejs/lib/anime.es.js';
import Splitting from 'splitting';
import MicroModal from 'micromodal';

const chars = Splitting({ by: "chars", whitespace: true });

document.addEventListener("DOMContentLoaded", function() {
  MicroModal.init({
    disableScroll: true,
    awaitCloseAnimation: true
  });

  if (window.innerWidth > 1080) {
    parallaxMouse();
  }

  function removeHash() {
    history.pushState("", document.title, window.location.pathname + window.location.search);
  }
});

window.addEventListener("load", () => {
  setTimeout(() => document.querySelector('html').scrollTo(0, 0), 1);
});
